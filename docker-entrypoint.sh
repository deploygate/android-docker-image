#!/bin/sh

cmd="$@"

if [ -z "$DISABLE_XVFB" ]; then
  export DISPLAY=:99

  Xvfb :99 -screen 0 1280x1024x24 &
fi

exec /bin/bash -lc "${cmd}"