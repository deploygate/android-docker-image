FROM circleci/android:api-27-alpha

MAINTAINER Jumpei Matsuda <jmatsu@deploygate.com>

# For rbenv
# Circleci image uses ruby-install
# https://github.com/circleci/circleci-images/blob/b1075968c8279567cdb74625bf73f7963de4d17d/android/Dockerfile.m4
RUN sudo apt-get install -y --force-yes --no-install-recommends build-essential curl git zlib1g-dev libssl-dev libreadline-dev libyaml-dev libxml2-dev libxslt-dev

RUN git clone https://github.com/sstephenson/rbenv.git /home/circleci/.rbenv && sleep 1 && git clone https://github.com/sstephenson/ruby-build.git /home/circleci/.rbenv/plugins/ruby-build

ENV PATH /home/circleci/.rbenv/bin:/home/circleci/.rbenv/shims:$PATH
RUN echo "install: --no-document\nupdate: --no-document" > ~/.gemrc

RUN rbenv install 2.5.1 && rbenv global 2.5.1
RUN gem install bundler

# if any other version is needed
# RUN rbenv install 2.5.1 && rbenv global 2.5.1
# RUN gem install bundler

COPY ./docker-entrypoint.sh /usr/local/bin/

# https://circleci.com/docs/2.0/custom-images/
LABEL com.circleci.preserve-entrypoint=true

ENTRYPOINT [ "docker-entrypoint.sh" ]
CMD ["/bin/sh"]
